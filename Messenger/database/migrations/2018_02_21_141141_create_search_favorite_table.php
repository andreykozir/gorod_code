<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchFavoriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('search_favorite');
        Schema::create('search_favorite', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->jsonb('filters');
            $table->string('messenger')->nullable();
            $table->timestamp('notification_last_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('counter')->default(0);
            $table->string('phone')->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_favorite');
    }
}
