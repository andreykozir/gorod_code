<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    Route::group([
            'namespace' => 'Cabinet',
            'prefix' => 'cabinet',
            'middleware' => 'auth',
        ],
        function () {
            Route::get('search/favorite', 'MySearchFavoriteController@get')
                ->name('search.get');

            Route::put('search/favorite', 'MySearchFavoriteController@update')
                ->name('search.update');

            Route::get('search/get-update-data', 'MySearchFavoriteController@getUpdateData')
                ->name('search.getUpdateData');

            Route::delete('search/favorite', 'MySearchFavoriteController@delete')
                ->name('search.delete');
        }
    );

    Route::post('search/favorite', 'SearchFavoriteController@create')
        ->name('search.create')
        ->middleware('auth');
});