<?php

namespace App\Events;

use App\Models\Realty\Realty;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RealtyCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /** @var Realty */
    private $realty;

    /**
     * Create a new event instance.
     *
     */
    public function __construct(Realty $realty)
    {
        $this->realty = $realty;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
    public function getRealty(): Realty
    {
        return $this->realty;
    }
}
