<?php

namespace App\DTOs\SearchFavorite;

class SearchFavoriteDTO
{
    /**
     * @var integer
     */
    private $userId;

    /**
     * @var array
     */
    private $filters;

    /**
     * @var string
     */
    private $messenger;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * UserManagerRegisterUserDto constructor.
     * @param integer $userId
     * @param array $filters
     * @param string $messenger
     * @param string $phone
     */
    public function __construct(int $userId, array $filters, string $messenger, string $phone = null)
    {
        $this->userId = $userId;
        $this->filters = $filters;
        $this->messenger = $messenger;
        $this->phone = $phone;
    }

    /**
     * @return integer
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return string
     */
    public function getMessanger(): string
    {
        return $this->messenger;
    }
    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

}