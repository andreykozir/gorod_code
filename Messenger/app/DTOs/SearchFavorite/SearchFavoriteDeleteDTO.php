<?php

namespace App\DTOs\SearchFavorite;

class SearchFavoriteDeleteDTO
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userId;

    /**
     * UserManagerRegisterUserDto constructor.
     * @param integer $id
     * @param integer $userId

     */
    public function __construct(int $id, int $userId)
    {
        $this->id = $id;
        $this->userId = $userId;
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @return integer
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}