<?php

namespace App\DTOs\SearchFavorite;

class SearchFavoriteUpdateDTO
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var string
     */
    private $messenger;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * UserManagerRegisterUserDto constructor.
     * @param integer $id
     * @param integer $userId
     * @param string $messenger
     * @param string $phone
     */
    public function __construct(int $id, int $userId, string $messenger, string $phone = null)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->messenger = $messenger;
        $this->phone = $phone;
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @return integer
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getMessenger(): string
    {
        return $this->messenger;
    }
    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }
}