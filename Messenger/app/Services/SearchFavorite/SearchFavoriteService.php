<?php

namespace App\Services\SearchFavorite;

use App\DTOs\SearchFavorite\SearchFavoriteDeleteDTO;
use App\DTOs\SearchFavorite\SearchFavoriteDTO;
use App\DTOs\SearchFavorite\SearchFavoriteUpdateDTO;
use App\Repositories\Repository\SearchFavoriteRepository;
use Illuminate\Database\Eloquent\Collection;

class SearchFavoriteService implements ISearchFavoriteService
{
    /** @var SearchFavoriteRepository */
    private $searchFavoriteRepository;

    /**
     * SearchFavoriteService constructor.
     * @param SearchFavoriteRepository $searchFavoriteRepository
     */
    public function __construct(SearchFavoriteRepository $searchFavoriteRepository)
    {
        $this->searchFavoriteRepository = $searchFavoriteRepository;
    }

    public function addInFavorite(SearchFavoriteDTO $searchFavoriteDTO): bool
    {
        if ($this->isFoundUserFilter($searchFavoriteDTO)) {
            return true;
        }

        return $this->searchFavoriteRepository->store($searchFavoriteDTO);
    }

    public function getUserSearches(int $userId): ?Collection
    {
        return $this->searchFavoriteRepository->getUserSearches($userId);
    }

    public function update(SearchFavoriteUpdateDTO $searchFavoriteUpdateDTO): bool
    {
        return $this->searchFavoriteRepository->update($searchFavoriteUpdateDTO);
    }

    public function isCorrectUserPhone(string $phone, ?array $phones): bool
    {
        if (!$phone) {
            return true;
        }
        //if phones is not set. new phone is not correct
        if (!$phones) {
            return false;
        }

        return in_array($phone, $phones, true);
    }

    public function deleteFavoriteSearch(SearchFavoriteDeleteDTO $searchFavoriteDeleteDTO): bool
    {
        return $this->searchFavoriteRepository->delete($searchFavoriteDeleteDTO);
    }

    private function isFoundUserFilter(SearchFavoriteDTO $searchFavoriteDTO): bool
    {
        return (bool)$this->searchFavoriteRepository->getCountUserRowOnFilter($searchFavoriteDTO);
    }
}