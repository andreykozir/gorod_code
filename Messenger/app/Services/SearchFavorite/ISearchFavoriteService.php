<?php

namespace App\Services\SearchFavorite;

use App\DTOs\SearchFavorite\SearchFavoriteDTO;
use App\DTOs\SearchFavorite\SearchFavoriteUpdateDTO;
use Illuminate\Database\Eloquent\Collection;
use App\DTOs\SearchFavorite\SearchFavoriteDeleteDTO;

interface ISearchFavoriteService
{
    public function addInFavorite(SearchFavoriteDTO $searchFavoriteDTO): bool;
    public function getUserSearches(int $userId): ?Collection;
    public function update(SearchFavoriteUpdateDTO $searchFavoriteUpdateDTO): bool;
    public function isCorrectUserPhone(string $phone, ?array $phones): bool;
    public function deleteFavoriteSearch(SearchFavoriteDeleteDTO $searchFavoriteDeleteDTO): bool;
}