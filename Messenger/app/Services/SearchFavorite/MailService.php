<?php

namespace App\Services\SearchFavorite;

use App\Helpers\Date\DateHelper;
use App\Mail\DefaultMail;
use App\Models\Search\SearchFavorite;
use App\Repositories\Repository\SearchFavoriteRepository;
use App\Services\Messenger\MessengerHelperService;
use App\Services\Realty\SearchService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Mail;

class MailService implements IMailService
{
    const MAX_NOTIFY = 1;

    /** @var SearchFavoriteRepository */
    private $searchFavoriteRepository;

    /** @var SearchService */
    private $searchService;

    /** @var MessengerHelperService */
    private $messengerHelperService;

    /** @var string */
    private $mailClass = DefaultMail::class;

    /**
     * MailService constructor.
     * @param SearchFavoriteService $searchFavoriteService
     * @param SearchFavoriteRepository $searchFavoriteRepository
     * @param MessengerHelperService $messengerHelperService
     */
    public function __construct(
        SearchFavoriteService $searchFavoriteService,
        SearchFavoriteRepository $searchFavoriteRepository,
        MessengerHelperService $messengerHelperService
    ) {
        $this->searchFavoriteRepository = $searchFavoriteRepository;
        $this->messengerHelperService = $messengerHelperService;
        $this->searchService = app(SearchService::class);
    }

    public function sendNotifications(): void
    {
        /** @var $searches SearchFavorite[] */
        $searches = $this->getSearchesForEmailNotification();
        if (!$searches) {
            return;
        }

        foreach ($searches as $search) {
            if (!$this->isNotify($search)) {
                continue;
            }

            $filters = $search->filters;

            $searchedRealty = $this->searchService->searchQuery($filters)
                ->whereBetween('created_at', [$search->notification_last_date, now()])->get();

            if ($searchedRealty->isEmpty()) {
                continue;
            }

            $this->sendMail($searchedRealty, $search);

            $this->increaseCountNotificationsMail($search);
        }
    }

    private function sendMail(Collection $realties, SearchFavorite $searchFavorite): bool
    {
        Mail::to($searchFavorite->user->email)
            ->queue(new $this->mailClass(
                [
                    'realtiesArray' => $this->messengerHelperService->getRealtyArrayWithAttributes($realties)
                ],
                [
                    'view' => 'emails.search_favorite_notification',
                    'subject' => __('searchFavorite.notification_title'),
                ]
            ));

        return empty(Mail::failures());
    }

    private function getSearchesForEmailNotification(): ?Collection
    {
        return $this->searchFavoriteRepository->getSearchForEmailNotification();
    }

    private function increaseCountNotificationsMail(SearchFavorite $searchFavorite): bool
    {
        if ($this->isNotifyDateIsNextWeek($searchFavorite)) {
            $searchFavorite->counter = 1;
        } else {
            ++$searchFavorite->counter;
        }
        $searchFavorite->notification_last_date = DateHelper::getTodayDate();
        return $searchFavorite->save();
    }

    private function isNotify(SearchFavorite $searchFavorite): bool
    {
        if ($this->isNotifyDateIsNextWeek($searchFavorite)) {
            return true;
        }
        return !$this->isMaxNotified($searchFavorite->counter);
    }

    private function isMaxNotified(int $count): bool
    {
        return $count >= self::MAX_NOTIFY;
    }

    private function isNotifyDateIsNextWeek(SearchFavorite $searchFavorite): bool
    {
        return Carbon::now() > $searchFavorite->notification_last_date->copy()->addWeek();
    }
}