<?php

namespace App\Services\SearchFavorite;

interface IMailService
{
    public function sendNotifications(): void;
}