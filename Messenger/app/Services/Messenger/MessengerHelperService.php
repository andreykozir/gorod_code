<?php

namespace App\Services\Messenger;

use App\Core\Behaviors\AttributeBehavior;
use App\Models\Realty\Realty;
use App\Services\Messenger\NotificationService\INotificationService;
use App\Services\Messenger\NotificationService\ViberNotificationService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Transformers\Realty\RealtyDefault;

class MessengerHelperService
{
    /** @const MESSENGERS list of the messengers*/
    private const MESSENGERS = [
        'viber' => [
            'name' => 'viber',
            'class' => ViberNotificationService::class
        ]
    ];

    private const NOTIFICATION_TIME_START_HOURS = 10;
    private const NOTIFICATION_TIME_END_HOURS = 19;

    public function isNotifyInCurrentTime(): bool
    {
        return $this->getNotificationStartTime() <= $this->getCurrentHours() && $this->getNotificationEndTime() >= $this->getCurrentHours();
    }

    public function isCorrectMessenger(string $messenger): bool
    {
        return isset(self::MESSENGERS[$messenger]);
    }
    public static function getDefaultSearchMessenger(): string
    {
        return self::MESSENGERS['viber']['name'];
    }
    public static function getMessengers(): array
    {
        return self::MESSENGERS;
    }
    public function getMessengerObjectOnName(string $messenger): ?INotificationService
    {
        if(!isset(self::MESSENGERS[$messenger]['class'])) {
            return null;
        }
        $messenger = self::MESSENGERS[$messenger]['class'];
        return new $messenger;
    }

    /**
     * Return attributes for realty.
     *
     * @param $realties Collection
     * @return array
     */
    public function getRealtyArrayWithAttributes(Collection $realties): array
    {
        $sendingRealties = [];
        foreach($realties as $realty) {
            $sendingRealties[$realty->id]['realty'] = (new RealtyDefault())->transform($realty);
            $sendingRealties[$realty->id]['attributes'] = $this->getMailAttributes($realty);
        }
        return $sendingRealties;
    }

    private function getMailAttributes(Realty $realty): array
    {
        $attributes = [];
        foreach($realty->realtyType->realtyAttributes as $attribute) {
            if($attribute->getContent(AttributeBehavior::NOTIFICATION_FIELD)) {
                $attributes[] = [
                    'realty' => $realty->toArray(),
                    'key' => $attribute->getContent(AttributeBehavior::TITLE_FIELD),
                    'value' => $realty->getContent($attribute->getContent(AttributeBehavior::NAME_FIELD)),
                ];
            }
        }

        return $attributes;
    }
    private function getNotificationStartTime(): int
    {
        return self::NOTIFICATION_TIME_START_HOURS;
    }
    private function getNotificationEndTime(): int
    {
        return self::NOTIFICATION_TIME_END_HOURS;
    }
    private function getCurrentHours(): int
    {
        return (new Carbon)->hour;
    }
}