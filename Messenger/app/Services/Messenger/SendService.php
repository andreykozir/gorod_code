<?php

namespace App\Services\Messenger;

use App\Models\Realty\Realty;
use App\Repositories\Repository\SearchFavoriteRepository;
use App\Services\SearchFavorite\SearchFavoriteService;
use App\Helpers\Date\DateHelper;
use App\Models\Search\SearchFavorite;
use App\Services\Realty\SearchService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;



class SendService
{
    /** @const MAX_NOTIFY_MESSENGER max count of the user notification */
    const MAX_NOTIFY_MESSENGER = 10;

    /** @var SearchFavoriteRepository */
    private $searchFavoriteRepository;

    /** @var SearchService|\Illuminate\Foundation\Application|mixed */
    private $searchService;

    /** @var MessengerHelperService */
    private $messengerHelperService;

    /**
     * SearchFavoriteService constructor.
     *
     * @var SearchFavoriteRepository $searchFavoriteRepository
     * @var MessengerHelperService $messengerHelperService
     */
    public function __construct(SearchFavoriteRepository $searchFavoriteRepository, MessengerHelperService $messengerHelperService) {
        $this->searchFavoriteRepository = $searchFavoriteRepository;
        $this->messengerHelperService = $messengerHelperService;
        $this->searchService = app(SearchService::class);
    }

    /**
     * Send notification on a messenger of the users.
     *
     * @param Realty $realty
     */
    public function sendNotifications(Realty $realty): void
    {
        $searchFavorites = $this->getSearchList();

        if (!$searchFavorites) {
            return;
        }

        foreach ($searchFavorites as $searchFavorite) {
            if (!$this->isNotify($searchFavorite)) {
                continue;
            }

            $filters = $searchFavorite->filters;
            $filters['id'] = $realty->id;

            $searchedRealty = $this->searchService->searchQuery($filters)->first();
            if (!$searchedRealty) {
                continue;
            }

            $messenger = $this->messengerHelperService->getMessengerObjectOnName($searchFavorite->messenger);
            if(!$messenger->send($searchedRealty)) {
                continue;

            }

            $this->increaseCountNotifications($searchFavorite);
        }
    }

    private function getSearchList(): ?Collection
    {
        return $this->searchFavoriteRepository->getSearches();
    }

    private function increaseCountNotifications(SearchFavorite $searchFavorite): bool
    {
        if (!$this->isNotificationDateToday($searchFavorite)) {
            $searchFavorite->notification_last_date = DateHelper::getTodayDate();
            $searchFavorite->counter = 1;
        } else {
            $searchFavorite->counter += 1;
        }
        return $searchFavorite->save();
    }

    private function isNotify(SearchFavorite $searchFavorite): bool
    {

        if (!$this->isNotificationDateToday($searchFavorite)) {
            return true;
        }
        return !$this->isMaxNotified($searchFavorite->counter);
    }

    private function isMaxNotified(int $count): bool
    {
        return $count >= self::MAX_NOTIFY_MESSENGER;
    }

    private function isNotificationDateToday(SearchFavorite $searchFavorite): bool
    {
        return $searchFavorite->notification_last_date->isToday();
    }
}