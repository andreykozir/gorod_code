<?php

namespace App\Services\Messenger\NotificationService;

use App\Models\Realty\Realty;

interface INotificationService
{
    public function send(Realty $realty): bool;
}