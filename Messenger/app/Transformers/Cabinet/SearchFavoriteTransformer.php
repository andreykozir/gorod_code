<?php

namespace App\Transformers\Cabinet;

use App\Models\Search\SearchFavorite;
use League\Fractal\TransformerAbstract;

class SearchFavoriteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     * @param SearchFavorite $searchFavorite
     * @return array
     */
    public function transform(SearchFavorite $searchFavorite): array
    {
        return [
            'id' => $searchFavorite->id,
            'filters' => $searchFavorite->filters,
            'messenger' => $searchFavorite->messenger,
            'phone' => $searchFavorite->phone
        ];
    }
}