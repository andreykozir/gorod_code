<?php

namespace App\Listeners;

use App\Events\RealtyCreated;
use App\Services\Messenger\MessengerHelperService;
use App\Services\Messenger\SendService;
use App\Services\Messenger\ViberService;
use App\Services\Realty\SearchService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRealtyNotification implements ShouldQueue
{
    use InteractsWithQueue;

    private $sendService;
    private $messengerHelperService;
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //TODO логика получения мессенджера по id messenger указанного у пользователя в notification в searchFavorite(В случае подключения других мессенджеров).
        $this->sendService = app(SendService::class);
        $this->messengerHelperService = new MessengerHelperService;
    }

    /**
     * Handle the event.
     *
     * @param  RealtyCreated $event
     * @return void
     */
    public function handle(RealtyCreated $event)
    {
        if(!$this->messengerHelperService->isNotifyInCurrentTime()) {
            return;
        }

        $realty = $event->getRealty();

        $this->sendService->sendNotifications($realty);
    }
}