<?php

namespace App\Repositories\Interfaces;

use App\DTOs\SearchFavorite\SearchFavoriteDeleteDTO;
use App\DTOs\SearchFavorite\SearchFavoriteDTO;
use App\DTOs\SearchFavorite\SearchFavoriteUpdateDTO;
use Illuminate\Database\Eloquent\Collection;

interface ISearchFavoriteRepository
{
    public function store(SearchFavoriteDTO $searchFavoriteDTO): bool;

    public function getCountUserRowOnFilter(SearchFavoriteDTO $searchFavoriteDTO): int;

    public function getUserSearches(int $userId): ?Collection;

    public function update(SearchFavoriteUpdateDTO $searchFavoriteUpdateDTO): bool;

    public function delete(SearchFavoriteDeleteDTO $searchFavoriteDeleteDTO): bool;

    public function getSearchForEmailNotification(): ?Collection;
}