<?php

namespace App\Repositories\Repository;

use App\DTOs\SearchFavorite\SearchFavoriteDeleteDTO;
use App\DTOs\SearchFavorite\SearchFavoriteDTO;
use App\DTOs\SearchFavorite\SearchFavoriteUpdateDTO;
use App\Models\Search\SearchFavorite;
use App\Repositories\Interfaces\ISearchFavoriteRepository;
use Illuminate\Database\Eloquent\Collection;

class SearchFavoriteRepository implements ISearchFavoriteRepository
{
    /**
     * @param SearchFavoriteDTO $searchFavoriteDTO
     * @return bool
     */
    public function store(SearchFavoriteDTO $searchFavoriteDTO): bool
    {
        $searchFavorite = SearchFavorite::create([
            'user_id' => $searchFavoriteDTO->getUserId(),
            'filters' => $searchFavoriteDTO->getFilters(),
            'messenger' => $searchFavoriteDTO->getMessenger(),
            'phone' => $searchFavoriteDTO->getPhone(),
        ]);
        return (bool)$searchFavorite;
    }

    /**
     * @param SearchFavoriteDTO $searchFavoriteDTO
     * @return int
     */
    public function getCountUserRowOnFilter(SearchFavoriteDTO $searchFavoriteDTO): int
    {
        return SearchFavorite::where('filters', '=', json_encode($searchFavoriteDTO->getFilters()))
            ->where('user_id', '=', $searchFavoriteDTO->getUserId())
            ->where('messenger', '=', $searchFavoriteDTO->getMessenger())
            ->count();
    }

    public function getUserSearches(int $userId): ?Collection
    {
        return SearchFavorite::where('user_id', '=', $userId)
            ->get();
    }

    public function getSearches(): ?Collection
    {
        return SearchFavorite::all();
    }

    public function update(SearchFavoriteUpdateDTO $searchFavoriteUpdateDTO): bool
    {
        $searchFavorite = $this->getUserSearchFavorite($searchFavoriteUpdateDTO->getId(), $searchFavoriteUpdateDTO->getUserId());
        if(!$searchFavorite) {
            return false;
        }
        $searchFavorite->messenger = $searchFavoriteUpdateDTO->getMessenger();
        $searchFavorite->phone = $searchFavoriteUpdateDTO->getPhone();
        return $searchFavorite->save();
    }

    public function delete(SearchFavoriteDeleteDTO $searchFavoriteDeleteDTO): bool
    {
        $searchFavorite = $this->getUserSearchFavorite($searchFavoriteDeleteDTO->getId(), $searchFavoriteDeleteDTO->getUserId());

        if(!$searchFavorite) {
            return true;
        }
        return $searchFavorite->delete();
    }

    public function getSearchForEmailNotification(): ?Collection
    {
        return SearchFavorite::with('user')
            ->whereNull('messenger')
            ->orWhereNull('phone')
            ->orderBy('user_id')
            ->get();
    }

    private function getUserSearchFavorite(int $id, int $userId): ?SearchFavorite
    {
        return SearchFavorite::where('id', '=', $id)
            ->where('user_id', '=', $userId)
            ->get()
            ->first();
    }
}