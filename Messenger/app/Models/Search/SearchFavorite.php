<?php

namespace App\Models\Search;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class SearchFavorite extends Model
{
    public $timestamps = false;

    protected $table = 'search_favorite';

    protected $casts = [
        'is_notifiable' => 'boolean',
        'filters' => 'array',
    ];

    protected $dates = [
        'notification_last_date'
    ];

    protected $fillable = [
        'user_id',
        'filters',
        'messenger',
        'phone',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}