<?php

namespace App\Http\Controllers\Api;

use App\DTOs\SearchFavorite\SearchFavoriteDTO;
use App\Http\Requests\Search\SearchFavoriteRequest;
use App\Services\Messenger\MessengerHelperService;
use App\Services\SearchFavorite\SearchFavoriteService;
use Illuminate\Http\JsonResponse;
use Response;

class SearchFavoriteController {

    /**
     * Add favorite in database
     *
     * @param SearchFavoriteRequest $request
     * @param SearchFavoriteService $searchFavoriteService
     * @return JsonResponse
     */
    public function create(SearchFavoriteRequest $request, SearchFavoriteService $searchFavoriteService): JsonResponse
    {
        $phones = auth()->user()->phone;

        //TODO change user phone
        $searchFavoriteDTO = new SearchFavoriteDTO(auth()->user()->id, $request->filters, MessengerHelperService::getDefaultSearchMessenger(), $phones[0] ?? null);
        if($searchFavoriteService->addInFavorite($searchFavoriteDTO)) {
            return Response::json(['success' => __('searchFavorite.stored')], 201);
        }
        return Response::json([
            'error' => __('searchFavorite.stored')
        ], 400);
    }
}