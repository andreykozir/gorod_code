<?php

namespace App\Http\Controllers\Api\Cabinet;

use App\DTOs\SearchFavorite\SearchFavoriteDeleteDTO;
use App\DTOs\SearchFavorite\SearchFavoriteUpdateDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cabinet\SearchFavoriteDeleteRequest;
use App\Http\Requests\Cabinet\SearchFavoriteUpdateRequest;
use App\Services\Messenger\MessengerHelperService;
use App\Services\SearchFavorite\SearchFavoriteService;
use App\Transformers\Cabinet\SearchFavoriteTransformer;
use Illuminate\Http\JsonResponse;
use Response;

class MySearchFavoriteController extends Controller
{
    public function get(SearchFavoriteService $searchFavoriteService): JsonResponse
    {
        $searches = $searchFavoriteService->getUserSearches(auth()->user()->id);
        return fractal($searches, new SearchFavoriteTransformer)->respond();
    }

    public function update(
        SearchFavoriteUpdateRequest $request,
        SearchFavoriteService $searchFavoriteService,
        MessengerHelperService $messengerHelperService
    ): JsonResponse {
        if (!$searchFavoriteService->isCorrectUserPhone($request->phone, auth()->user()->phone)) {
            return Response::json(['error' => __('searchFavorite.phone_is_not_correct')], 400);
        }
        if (!$messengerHelperService->isCorrectMessenger($request->messenger)) {
            return Response::json(['error' => __('searchFavorite.messenger_is_not_correct')], 400);
        }

        $searchFavoriteUpdateDTO = new SearchFavoriteUpdateDTO($request->id, auth()->user()->id, $request->messenger,
            $request->phone);

        if ($searchFavoriteService->update($searchFavoriteUpdateDTO)) {
            return Response::json(['success' => __('searchFavorite.updated')], 200);
        }

        return Response::json(['error' => __('searchFavorite.update_error')], 400);
    }

    public function getUpdateData(): JsonResponse
    {
        return Response::json([
            'messengers' => MessengerHelperService::getMessengers(),
            'phones' => auth()->user()->phone,
        ], 400);
    }

    public function delete(
        SearchFavoriteDeleteRequest $request,
        SearchFavoriteService $searchFavoriteService
    ): JsonResponse {
        $searchFavoriteUpdateDTO = new SearchFavoriteDeleteDTO($request->id, auth()->user()->id);
        if ($searchFavoriteService->deleteFavoriteSearch($searchFavoriteUpdateDTO)) {
            return Response::json(['success' => __('searchFavorite.deleted')], 200);
        }
        return Response::json(['error' => __('searchFavorite.delete_error')], 400);
    }
}