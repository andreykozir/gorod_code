<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('comments');

        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')
                ->nullable();
            $table->integer('user_social_id')
                ->nullable();

            $table->integer('parent_id')
                ->nullable();

            $table->string('entity_type');
            $table->string('avatar')
                ->nullable();
            $table->integer('entity_id');

            $table->integer('citation_comment_id')
                ->nullable();

            $table->boolean('approved')
                ->default(0);

            $table->text('message');

            $table->string('full_name')
                ->nullable();
            $table->string('email')
                ->nullable();
            $table->string('home_page')
                ->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('user_social_id')
                ->references('id')
                ->on('user_social')
                ->onDelete('set null');

            $table->foreign('parent_id')
                ->references('id')
                ->on('comments')
                ->onDelete('cascade');

            $table->foreign('citation_comment_id')
                ->references('id')
                ->on('comments')
                ->onDelete('cascade');

            $table->foreign('user_social_id')
                ->references('id')
                ->on('user_social')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
