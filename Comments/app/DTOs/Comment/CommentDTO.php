<?php

namespace App\DTOs\Comment;

class CommentDTO
{
    /**
     * @var string
     */
    private $entityType;
    /**
     * @var integer
     */
    private $entityId;
    /**
     * @var string
     */
    private $message;
    /**
     * @var integer
     */
    private $parentId;
    /**
     * @var integer
     */
    private $userId;
    /**
     * @var integer
     */
    private $citationCommentId;
    /**
     * @var string
     */
    private $fullName;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $homePage;

    /**
     * UserManagerRegisterUserDto constructor.
     * @param integer $parentId
     * @param string $entityType
     * @param integer $entityId
     * @param string $message
     * @param integer $userId
     * @param integer $citationCommentId
     * @param string $fullName
     * @param string $email
     * @param string $homePage
     */
    public function __construct(
        string $entityType,
        int $entityId,
        string $message,
        int $parentId = null,
        int $userId = null,
        int $citationCommentId = null,
        string $fullName = null,
        string $email = null,
        string $homePage = null
    ) {
        $this->entityType = $entityType;
        $this->entityId = $entityId;
        $this->message = $message;
        $this->parentId = $parentId;
        $this->userId = $userId;
        $this->citationCommentId = $citationCommentId;
        $this->fullName = $fullName;
        $this->email = $email;
        $this->homePage = $homePage;
    }

    /**
     * @return string
     */
    public function getEntityType(): string
    {
        return $this->entityType;
    }

    /**
     * @return integer
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return integer
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @return integer
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return integer
     */
    public function getCitationCommentId(): ?int
    {
        return $this->citationCommentId;
    }

    /**
     * @return string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getHomePage(): ?string
    {
        return $this->homePage;
    }
}