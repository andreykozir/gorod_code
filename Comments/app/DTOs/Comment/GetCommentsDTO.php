<?php

namespace App\DTOs\Comment;

class GetCommentsDTO
{
    /**
     * @var string
     */
    private $entityType;
    /**
     * @var integer
     */
    private $entityId;

    /**
     * UserManagerRegisterUserDto constructor.
     * @param string $entityType
     * @param integer $entityId
     */
    public function __construct(string $entityType, int $entityId)
    {
        $this->entityType = $entityType;
        $this->entityId = $entityId;
    }

    /**
     * @return string
     */
    public function getEntityType(): string
    {
        return $this->entityType;
    }

    /**
     * @return integer
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }
}