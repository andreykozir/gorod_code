<?php

namespace App\DTOs\Social;

class UserDTO
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $socialId;

    /**
     * @var string
     */
    private $socialType;

    /**
     * @var string
     */
    private $avatar;

    /**
     * UserManagerRegisterUserDto constructor.
     * @param string $name
     * @param string $socialId
     * @param string $socialType
     * @param string $avatar
     */
    public function __construct(string $name, string $socialId, string $socialType, string $avatar)
    {
        $this->name = $name;
        $this->socialId = $socialId;
        $this->socialType = $socialType;
        $this->avatar = $avatar;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSocialId(): string
    {
        return $this->socialId;
    }
    /**
     * @return string
     */
    public function getSocialType(): string
    {
        return $this->socialType;
    }
    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

}