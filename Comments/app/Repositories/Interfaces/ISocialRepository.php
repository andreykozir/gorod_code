<?php

namespace App\Repositories\Interfaces;

use App\DTOs\Social\UserDTO;
use App\Models\Social\UserSocial;

Interface ISocialRepository
{
    public function find(UserDTO $userDTO): ?UserSocial;
    public function storage(UserDTO $userDTO): UserSocial;
    public function deleteUserOnId(int $id): bool;
}