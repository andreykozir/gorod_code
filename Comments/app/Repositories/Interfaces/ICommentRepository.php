<?php

namespace App\Repositories\Interfaces;

use App\DTOs\Comment\CommentDTO;
use App\DTOs\Comment\GetCommentsDTO;
use App\Models\Comment\Comment;
use Illuminate\Pagination\LengthAwarePaginator;

interface ICommentRepository
{
    public function storage(CommentDTO $commentDTO) : ?Comment;
    public function getCommentsByEntityType(GetCommentsDTO $getCommentsDTO): ?LengthAwarePaginator;
    public function setUserSocialToComment(int $commentId, int $userSocialId) : bool;
    public function getCommentById(int $id): ?Comment;
    public function deleteCommentById(int $id): bool;
    public function cntCommentByUserSocialId(int $userSocialId): int;
}