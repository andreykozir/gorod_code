<?php

namespace App\Repositories\Repository;

use App\DTOs\Comment\CommentDTO;
use App\DTOs\Comment\GetCommentsDTO;
use App\Models\Comment\Comment;
use App\Repositories\Interfaces\ICommentRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class CommentRepository implements ICommentRepository
{
    public function storage(CommentDTO $commentDTO): ?Comment
    {
        return Comment::create([
            'entity_type' => $commentDTO->getEntityType(),
            'entity_id' => $commentDTO->getEntityId(),
            'message' => $commentDTO->getMessage(),
            'parent_id' => $commentDTO->getParentId(),
            'user_id' => $commentDTO->getUserId(),
            'citation_comment_id' => $commentDTO->getCitationCommentId(),
            'full_name' => $commentDTO->getFullName(),
            'email' => $commentDTO->getEmail(),
            'home_page' => $commentDTO->getHomePage(),
        ]);
    }

    public function getCommentsByEntityType(GetCommentsDTO $getCommentsDTO): ?LengthAwarePaginator
    {
        return Comment::where('entity_id', '=', $getCommentsDTO->getEntityId())
            ->where('entity_type', '=', $getCommentsDTO->getEntityType())
            ->where('approved', '=', true)
            ->with('user', 'user_social', 'citation.user', 'parent.user')
            ->paginate();
    }

    /**
     * Set id in user comment
     *
     * @param int $commentId
     * @param int $userSocialId
     * @return bool
     */
    public function setUserSocialToComment(int $commentId, int $userSocialId): bool
    {
        $comment = Comment::find($commentId);
        if (!$comment) {
            return false;
        }
        $comment->user_social_id = $userSocialId;
        return $comment->save();
    }

    /**
     * Get comment on id
     *
     * @param int $id
     * @return Comment|null
     */
    public function getCommentById(int $id): ?Comment
    {
        return Comment::find($id);
    }

    /**
     * Delete comment on id
     *
     * @param int $id
     * @return bool
     */
    public function deleteCommentById(int $id): bool
    {
        return Comment::find($id)
            ->delete();
    }

    /**
     * Return count of the user comments
     *
     * @param int $userSocialId
     * @return int
     */
    public function cntCommentByUserSocialId(int $userSocialId): int
    {
        return Comment::where('user_social_id', '=', $userSocialId)
            ->count();
    }
}