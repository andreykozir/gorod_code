<?php

namespace App\Repositories\Repository;

use App\DTOs\Social\UserDTO;
use App\Models\Social\UserSocial;
use App\Repositories\Interfaces\ISocialRepository;

class SocialRepository implements ISocialRepository
{
    public function find(UserDTO $userDTO): ?UserSocial
    {
        return UserSocial::where('social_id', '=', (string)$userDTO->getSocialId())
            ->where('social_type', '=', $userDTO->getSocialType())
            ->first();
    }
    public function storage(UserDTO $userDTO): UserSocial
    {
        return UserSocial::create([
            'social_id' => $userDTO->getSocialId(),
            'social_type' => $userDTO->getSocialType(),
            'name' => $userDTO->getName(),
            'avatar' => $userDTO->getAvatar(),
        ]);
    }
    public function deleteUserOnId(int $id): bool
    {
        return UserSocial::find($id)->delete();
    }
}