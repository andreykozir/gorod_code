<?php

namespace App\Http\Requests\Comment;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id' => 'integer|nullable|exists:comments,id',
            'entity_id' => 'required|integer',
            'message' => 'required|string',
            'citation_comment_id' => 'integer|nullable|exists:comments,id',
            'full_name' => 'nullable|string',
            'email' => 'nullable|string|email',
            'home_page' => 'nullable|string|active_url',
        ];
    }
}
