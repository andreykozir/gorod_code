<?php

namespace App\Http\Controllers\Api;

use App\Core\Behaviors\AbstractMany;
use App\DTOs\Comment\CommentDTO;
use App\DTOs\Comment\GetCommentsDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Comment\CommentRequest;
use App\Services\Comment\CommentService;
use App\Transformers\Comment\CommentTransformer;
use Illuminate\Http\JsonResponse;
use Response;

class CommentController extends Controller
{
    public function create(AbstractMany $type, CommentRequest $request, CommentService $commentService): JsonResponse
    {
        $userId = $request->user() ? $request->user()->id : null;

        $commentDTO = new CommentDTO($type->slug, $request->entity_id, $request->message, $request->parent_id, $userId,
            $request->citation_comment_id, $request->full_name, $request->email, $request->home_page);

        $comment = $commentService->storage($commentDTO);
        if ($comment) {
            return Response::json([
                'id' => $comment->id,
                'success' => __('comment.created')
            ], 201);
        }
        return Response::json(['error' => __('comment.create_error')], 400);
    }

    public function get(AbstractMany $type, int $entityId, CommentService $commentService): JsonResponse
    {
        $commentDTO = new GetCommentsDTO($type->slug, $entityId);
        $comments = $commentService->getCommentsByEntityType($commentDTO);

        if (!$comments) {
            return Response::json([], 204);
        }

        $data = [
            'total' => $comments->total(),
            'page' => $comments->currentPage(),
            'perPage' => $comments->perPage(),
            'items' => fractal($comments->items(), new CommentTransformer())->toArray(),
        ];

        return Response::json($data, 200);
    }

    public function delete(int $id, CommentService $commentService): JsonResponse
    {
        $isDelete = $commentService->deleteCommentById($id);
        if ($isDelete) {
            return Response::json(['success' => __('comment.deleted')], 200);
        }
        return Response::json(['error' => __('comment.delete_error')], 400);
    }
}