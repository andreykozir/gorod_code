<?php

namespace App\Http\Controllers\Social;

use App\Http\Controllers\Controller;
use App\Services\Social\SocialService;
use Illuminate\Http\RedirectResponse;
use URL;

class SocialController extends Controller
{
    /**
     * Redirect the user to an authentication page.
     *
     * @param string $social
     * @param SocialService $socialService
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectToProvider(string $social, SocialService $socialService): RedirectResponse
    {
        $callBackData = request()->get('data');

        $socialService->setCallback('App\Services\Social\SocialCallbackService', 'callableMethod', $callBackData);
        $socialService->setReferUrl(URL::previous());

        return $socialService->redirectToProvider($social);
    }

    /**
     * Obtain the user information from Social network.
     *
     * @param string $social
     * @param SocialService $socialService
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback(string $social, SocialService $socialService): RedirectResponse
    {
        $user = $socialService->auth($social);
        return $socialService->handleCallback($user);
    }
}