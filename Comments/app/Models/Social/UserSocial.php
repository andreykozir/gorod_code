<?php

namespace App\Models\Social;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    protected $table = 'user_social';

    protected $fillable = [
        'social_id',
        'social_type',
        'name',
        'avatar'
    ];
}