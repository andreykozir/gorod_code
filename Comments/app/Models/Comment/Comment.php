<?php

namespace App\Models\Comment;

use App\Models\Social\UserSocial;
use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\DateHelper;

class Comment extends Model
{
    protected $table = 'comments';

    protected $fillable = [
        'entity_type',
        'entity_id',
        'message',
        'parent_id',
        'user_id',
        'citation_comment_id',
        'full_name',
        'email',
        'home_page',
    ];

    protected $casts = [
        'approved' => 'boolean',
    ];

    public function getCreatedAtAttribute($date): string
    {
        return DateHelper::transformDate($date);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function parent()
    {
        return $this->hasOne(Comment::class, 'id', 'parent_id');
    }
    public function citation()
    {
        return $this->hasOne(Comment::class, 'id', 'citation_comment_id');
    }

    public function user_social()
    {
        return $this->hasOne(UserSocial::class, 'id', 'user_social_id');
    }
}
