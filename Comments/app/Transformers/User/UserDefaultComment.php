<?php

namespace App\Transformers\User;

use App\Models\Comment\Comment;
use App\Transformers\AbstractTransformer;

class UserDefaultComment extends AbstractTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param Comment $comment
     * @return array
     */
    public function transform(Comment $comment): array
    {
        $image = asset('/img/anonymous.png');
        return [
            'id' => null,
            'role' => __('Anonymous'),
            'image' => $image,
            'name' => $comment->full_name,
        ];
    }
}
