<?php

namespace App\Transformers\User;

use App\Models\Social\UserSocial;
use App\Transformers\AbstractTransformer;

class UserSocialComment extends AbstractTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param UserSocial $userSocial
     * @return array
     */
    public function transform(UserSocial $userSocial): array
    {
        $image = asset('/img/anonymous.png');
        return [
            'id' => null,
            'role' => __('Social'),
            'name' => $userSocial->name,
            'image' => $image,
        ];
    }
}
