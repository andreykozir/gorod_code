<?php

namespace App\Transformers\User;

use App\User;
use League\Fractal\TransformerAbstract;

class UserDefault extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'name' => $user->name,
            'surname' => $user->surname,
            'patronymic' => $user->patronymic,
            'email' => $user->email,
            'phone' => $user->phone
        ];
    }
}
