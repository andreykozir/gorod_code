<?php
/**
 * Created by PhpStorm.
 * User: grothentor
 * Date: 1/11/18
 * Time: 2:24 PM
 */

namespace App\Transformers\User;

use App\Transformers\AbstractTransformer;
use App\User;
use Orchid\Platform\Core\Models\Role;

class UserComment extends AbstractTransformer
{
    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @param bool $forJson
     * @return array
     */
    public function transform(User $user = null, $forJson = true)
    {
        $this->setForJson($forJson);
        $image = asset('/img/anonymous.png');
        if (!$user) {
            return [
                'id' => null,
                'role' => __('Anonymous'),
                'name' => __('Anonymous'),
                'image' => $image,
            ];
        }

        $userStructure = [
            'id',
            'name' => function (User $user) {
                return "$user->name $user->surname";
            },
            'role' => function (User $user) {
                $role = $user->getRoles()->reduce(function ($roles, Role $role) {
                    if ($roles) {
                        $roles .= ", $role->name";
                    } else {
                        $roles = $role->name;
                    }
                    return $roles;
                }, null);
                return  $role ?: __('User');
            },
            'image' => function (User $user) use($image) {
                return $image;
            },
        ];

        $userObject = $this->parseData($user, $userStructure);
        return $userObject;
    }
}
