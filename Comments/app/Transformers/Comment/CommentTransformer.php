<?php

namespace App\Transformers\Comment;

use App\Models\Comment\Comment;
use App\Transformers\User\UserComment;
use App\Transformers\User\UserDefaultComment;
use App\Transformers\User\UserSocialComment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * @var UserComment
     */
    private $userComment;

    /**
     * @var UserSocialComment
     */
    private $userSocialComment;

    /**
     * @var UserDefaultComment
     */
    private $userDefaultComment;

    public function __construct()
    {
        $this->userComment = new UserComment();
        $this->userSocialComment = new UserSocialComment();
        $this->userDefaultComment = new UserDefaultComment();
    }

    /**
     * A Fractal transformer.
     *
     * @param Comment $comment
     * @return array
     */
    public function transform(Comment $comment): array
    {
        return [
            'id' => $comment->id,
            'parent_id' => $comment->parent_id,
            'message' => $comment->message,
            'entity_type' => $comment->entity_type,
            'entity_id' => $comment->entity_id,
            'created_at' => $comment->created_at,
            'user' => $this->transformUserData($comment),
            'citation' => $comment->citation ? $this->transformCitation($comment->citation) : null,
            'home_page' => $comment['home_page'],
        ];
    }

    /**
     * Transform data of the citation.
     *
     * @param Comment $citation
     * @return array
     */
    private function transformCitation(Comment $citation): array
    {
        return [
            'message' => $citation->message,
            'user' => $this->transformUserData($citation)
        ];
    }

    /**
     * Transform data of the user.
     *
     * @param Comment $comment
     * @return array
     */
    private function transformUserData(Comment $comment): array
    {
        if ($comment->user) {
            return (array)$this->userComment->transform($comment->user, false);
        }

        if ($comment->user_social) {
            return $this->userSocialComment->transform($comment->user_social);
        }
        return $this->userDefaultComment->transform($comment);
    }
}
