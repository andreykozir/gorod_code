<?php

namespace App\Services\Comment;

use App\DTOs\Comment\CommentDTO;
use App\DTOs\Comment\GetCommentsDTO;
use App\Models\Comment\Comment;
use App\Repositories\Repository\CommentRepository;
use App\Repositories\Repository\SocialRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;

class CommentService implements ICommentService
{
    private $commentRepository;
    private $socialRepository;

    public function __construct(CommentRepository $commentRepository, SocialRepository $socialRepository)
    {
        $this->commentRepository = $commentRepository;
        $this->socialRepository = $socialRepository;

    }

    public function storage(CommentDTO $commentDTO): ?Comment
    {
        $comment = $this->commentRepository->storage($commentDTO);
        if(!$comment) {
            return null;
        }
        $this->addCommentIdInSession($comment->id);
        return $comment;
    }

    public static function hasCommentIdInSession(int $commentId): bool
    {
        $writtenComments = Session::get('written_comments');
        if(!$writtenComments) {
            return false;
        }
        return in_array($commentId, $writtenComments, true);
    }


    public function getCommentsByEntityType(GetCommentsDTO $getCommentsDTO): ?LengthAwarePaginator
    {
        return $this->commentRepository->getCommentsByEntityType($getCommentsDTO);
    }

    public function deleteCommentById(int $id): bool
    {
        $comment = $this->commentRepository->getCommentById($id);

        if(!$comment) {
            return false;
        }

        //to array before deleting of the comment
        $comment = $comment->toArray();

        if(!$this->commentRepository->deleteCommentById($id)) {
            return false;
        }

        if(!$this->isDeletingUser($comment['user_social_id'])) {
            return true;
        }

        return $this->socialRepository->deleteUserOnId($comment['user_social_id']);
    }
    private function addCommentIdInSession(int $commentId): void
    {
        Session::push('written_comments', $commentId);
    }
    private function isDeletingUser(int $userSocialId = null): bool
    {
        return $userSocialId && $this->commentRepository->cntCommentByUserSocialId($userSocialId) === 0;
    }
}
