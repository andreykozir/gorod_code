<?php

namespace App\Services\Comment;

use App\DTOs\Comment\CommentDTO;
use App\DTOs\Comment\GetCommentsDTO;
use App\Models\Comment\Comment;
use Illuminate\Pagination\LengthAwarePaginator;

interface ICommentService {

    public function storage(CommentDTO $commentDTO): ?Comment;
    public static function hasCommentIdInSession(int $commentId);
    public function getCommentsByEntityType(GetCommentsDTO $getCommentsDTO): ?LengthAwarePaginator;
    public function deleteCommentById(int $id): bool;

}
