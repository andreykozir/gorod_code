<?php

namespace App\Services\Social;

use App\Models\Social\UserSocial;
use App\Repositories\Repository\CommentRepository;
use App\Services\Comment\CommentService;

class SocialCallbackService
{
    /**
     * const TYPE_COMMENT
     */
    const TYPE_COMMENT = 'comment';

    /**
     * @var CommentRepository
     */
    private $commentRepository;

    public function __construct()
    {
        $this->commentRepository = new CommentRepository;
    }

    /**
     * Callable method after social authorization.
     *
     * @param UserSocial $user
     * @param array|null $data
     * @return bool
     */
    public function callableMethod(UserSocial $user, array $data = null): bool
    {
        //to the type of comments and user is author of the comment.
        if ($this->isTypeComment($data['type']) && CommentService::hasCommentIdInSession($data['id'])) {
            return $this->commentRepository->setUserSocialToComment($data['id'], $user->id);
        }
        return false;
    }

    /**
     * Type of the callback is comment
     *
     * @param $type
     * @return bool
     */
    private function isTypeComment($type): bool
    {
        return $type === self::TYPE_COMMENT;
    }
}