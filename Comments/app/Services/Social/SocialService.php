<?php

namespace App\Services\Social;

use App\DTOs\Social\UserDTO;
use App\Models\Social\UserSocial;
use App\Repositories\Repository\SocialRepository;
use Illuminate\Http\RedirectResponse;
use Laravel\Socialite\AbstractUser;
use Socialite;
use Session;


class SocialService
{
    /**
     * @const SOCIAL - data on social network.
     */
    private const SOCIAL = [
        'facebook' => [
            'social_type' => 'fb'
        ],
        'twitter' => [
            'social_type' => 'twitter'
        ],
        'google' => [
            'social_type' => 'google'
        ]
    ];

    /**
     * @var SocialRepository
     */
    private $socialRepository;

    /**
     * SocialService constructor.
     * @param SocialRepository $socialRepository
     */
    public function __construct(SocialRepository $socialRepository)
    {
        $this->socialRepository = $socialRepository;
    }

    /**
     * Redirect to social provider
     *
     * @param string $provider
     * @return RedirectResponse
     */
    public function redirectToProvider(string $provider): RedirectResponse
    {
        if (!$this->isCorrectDriver($provider)) {
            abort(404);
        }

        return Socialite::driver($provider)
            ->redirect();
    }

    /**
     * Set the callback witch need call after authorization.
     *
     *
     * @param string $className
     * @param string $methodName
     * @param array $callbackData
     *
     * @return void
     */
    public function setCallback(string $className, string $methodName, array $callbackData = null): void
    {
        Session::put('socialCallback', [
            'class' => $className,
            'method' => $methodName,
            'data' => $callbackData
        ]);
    }

    /**
     * Set the callback witch need call after authorization.
     *
     * @return void
     */
    public function setReferUrl(string $url): void
    {
        Session::put('socialReferUrl', $url);
    }


    /**
     * Return the callback witch need call after authorization.
     *
     * @return array|null
     */
    public function getCallback(): ?array
    {
        return Session::has('socialCallback') ? Session::get('socialCallback') : null;
    }

    /**
     * Authorization or registration on the user data. Reception object of a user or null.
     *
     * @param string $driver
     * @return \App\Models\Social\UserSocial
     */
    public function auth(string $driver): ?UserSocial
    {
        if (!$this->isCorrectDriver($driver)) {
            abort(404);
        }

        try {
            $user = $this->getUserOnDriver($driver);
        } catch (\Exception $e) {
            return null;
        }

        $userDTO = new UserDTO($user->getName(), $user->getId(), $this->getSocialDBType($driver), $user->getAvatar());

        $user = $this->socialRepository->find($userDTO);

        if ($user) {
            return $user;
        }

        return $this->socialRepository->storage($userDTO);
    }

    /**
     * Handle callback function from the session.
     *
     * @param UserSocial $userSocial
     * @return RedirectResponse
     */
    public function handleCallback(UserSocial $userSocial = null): RedirectResponse
    {
        $callback = $this->getCallback();

        if (!$callback || !$this->isCorrectCallback($callback)) {
            return $this->redirectToDefaultRout();
        }

        if (!$userSocial) {
            return $this->hasReferUrl() ? $this->redirectToReferUrlWithError(__('comment.user_not_found')) : $this->redirectToDefaultRout();
        }

        try {
            $reflectionMethod = new \ReflectionMethod($callback['class'], $callback['method']);
            $result = $reflectionMethod->invoke(new $callback['class'], $userSocial, $callback['data']);
        } catch (\ReflectionException $exception) {
            return $this->redirectToReferUrlWithError(__('comment.auth_error'));
        }

        $referUrl = $this->getReferUrl();

        if (!$referUrl) {
            return $this->redirectToDefaultRout();
        }

        if ($result) {
            return $this->redirectToReferUrl();
        }
        return $this->redirectToDefaultRout();
    }

    /**
     * Set the callback witch need call after authorization.
     *
     * @return string
     */
    private function getReferUrl(): string
    {
        return Session::has('socialReferUrl') ? Session::get('socialReferUrl') : '';
    }

    private function hasReferUrl(): bool
    {
        return (bool)$this->getReferUrl();
    }

    private function redirectToReferUrlWithError(string $text): RedirectResponse
    {
        $refer = $this->getReferUrl();
        return redirect($refer)->withErrors(['errors' => ['user' => $text]]);
    }

    private function redirectToReferUrl(): RedirectResponse
    {
        $refer = $this->getReferUrl();
        return redirect($refer);
    }

    /**
     * Whether the class name and method name are correct
     *
     * @param $callback
     * @return bool
     */
    private function isCorrectCallback($callback): bool
    {
        return class_exists($callback['class']) && method_exists($callback['class'], $callback['method']);
    }

    /**
     * Is correct social driver
     *
     * @param string $driver
     * @return bool
     */
    private function isCorrectDriver(string $driver): bool
    {
        return array_key_exists($driver, self::SOCIAL);
    }

    /**
     * Get user on name of a driver
     *
     * @param $driver
     * @return AbstractUser
     */
    private function getUserOnDriver(string $driver): AbstractUser
    {
        return Socialite::driver($driver)->user();
    }

    /**
     * Social type in database
     *
     * @param $driver
     * @return string
     */
    private function getSocialDBType($driver): string
    {
        return self::SOCIAL[$driver]['social_type'];
    }

    private function redirectToDefaultRout(): RedirectResponse
    {
        return redirect()->route('home');
    }
}
