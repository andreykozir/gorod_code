<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/social/login/{social}', 'Social\SocialController@redirectToProvider')
    ->name('social.login')
    ->where(['social' => '[a-z]+']);

Route::get('/social/logged/{social}', 'Social\SocialController@handleProviderCallback')
    ->name('social.logged')
    ->where(['social' => '[a-z]+']);