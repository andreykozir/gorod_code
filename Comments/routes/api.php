<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {

    Route::group([
        'prefix' => 'comments',
    ],
        function () {
            Route::get('{type}/{entityId}', 'CommentController@get')
                ->where([
                    'entityId' => '[0-9]+'
                ])
                ->name('comments.index');

            Route::post('{type}', 'CommentController@create')
                ->where(['name' => '[a-z]+'])
                ->name('comments.create');

            Route::delete('{id}', 'CommentController@delete')
                ->where(['name' => '[0-9]+'])
                ->name('comments.delete');
        }
    );
});